"""web1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'$', views.page_start1, name="page_start1"),
    url(r'^start2', views.page_start2, name="page_start2"),
    url(r'^login', views.page_login, name="page_login"),
    url(r'^proc_login', views.proc_login, name="proc_login"),
    url(r'^proc_logout', auth_views.LogoutView.as_view(), name="logout"),
    url(r'^signup/$', views.signup, name='signup'),

]