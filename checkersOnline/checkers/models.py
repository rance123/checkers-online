# -*- coding: UTF-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=User)
def new_user(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_(sender, instance, **kwargs):
    instance.profile.save()


class Skins(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)

    class Meta:
        db_table = "Skins"


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    exp = models.IntegerField(null=True)
    money = models.IntegerField(null=True)
    skin = models.ForeignKey(Skins, to_field='id', on_delete=models.CASCADE,null=True)
    winCount = models.IntegerField(null=True)
    gamesCount = models.IntegerField(null=True)

    class Meta:
        db_table = "Player"


class Game(models.Model):
    id = models.IntegerField(primary_key=True)
    player1 = models.ManyToManyField(Profile)
    bet = models.IntegerField()
    result = models.BooleanField()

    class Meta:
        db_table = "Game"



