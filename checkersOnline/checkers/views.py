from django.http import *
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm


def page_start1(request):
    template = loader.get_template("start1.html")
    return HttpResponse(template.render())


def page_start2(request):
    template = loader.get_template("start2.html")
    return HttpResponse(template.render())


def page_login(request):
    template = loader.get_template("login.html")
    return HttpResponse(template.render())


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('page_login')
    else:
        form = UserCreationForm()
        return render(request, 'signup.html', {'form': form})


def proc_login(request):
    user_name = request.GET['user_name']
    password = request.GET['password']
    user = authenticate(username=user_name, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect("/start2/")
        else:
            return redirect("/login/")
        return redirect("/login/")
