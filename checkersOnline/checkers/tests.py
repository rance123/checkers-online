from django.test import TestCase

from checkers.models import *
from django.contrib.auth.models import User


class TestPlayerSkinGame(TestCase):

    def testDependencies(self):
        test_user1 = User.objects.create_user(username='fd92fduf', email='1@mail.ru', password='12345')
        test_user2 = User.objects.create_user(username='fko2dfk', email='2@mail.ru', password='78945')
        game1 = Game.objects.create(id=1, bet=150, result=0)
        game2 = Game.objects.create(id=2, bet=2000, result=1)
        game1.player1.add(1)
        game2.player1.add(2)
        gametest1 = Game.objects.get(player1=test_user1.id)
        gametest2 = Game.objects.get(player1=test_user2.id)
        self.assertEqual(gametest1.bet, 150)
        self.assertEqual(gametest2.result, 1)



class UsersLoginTest(TestCase):

    def setUp(self):
        test_user3 = User.objects.create_user(username='fd9fduf', email='1@mail.ru', password='12345')
        test_user4 = User.objects.create_user(username='fkodfk', email='2@mail.ru', password='78945')
        test_admin = User.objects.create_user(username='admin', email='3@mail.ru',
                                              password='test', is_superuser=1, is_staff=1)
        test_not_admin = User.objects.create_user(username='noadmin', email='4@mail.ru',
                                                  password='test2', is_superuser=0, is_staff=0)

    def test_login_users(self):
        login1 = (self.client.login(username='fd9fduf', password='12345'))
        self.assertTrue(login1)
        login2 = (self.client.login(username='fkodfk', password='99999'))
        self.assertFalse(login2)

    def test_login_admin(self):
        self.client.login(username='admin', password='test')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)

    def test_login_not_admin(self):
        self.client.login(username='noadmin', password='test2')
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)

